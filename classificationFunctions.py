# classification code library
# updated 6/1/2018

def plotCategoryCounts(articlesDF, grouping = 'category'):
    import matplotlib.pyplot as plt
    
    groupCounts = articlesDF.groupby(grouping).articles.count()
    totalCount = sum(groupCounts) 
    print([round(x / totalCount,3) for x in groupCounts])
    
    fig = plt.figure(figsize=(8,6))
    articlesDF.groupby(grouping).articles.count().plot.bar(ylim=0)
    plt.title(str(totalCount) + ' articles')
    plt.show()

from sklearn.feature_extraction.text import TfidfVectorizer

def getVocabulary(articles, categories):
    tfidf = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1', ngram_range=(1, 2), stop_words='english')
    features = tfidf.fit_transform(articles).toarray()
    labels = categories

    print('vocab indices range from ' + 
          str(min([c for (x,c) in tfidf.vocabulary_.items()])) + ' to ' +
          str(max([c for (x,c) in tfidf.vocabulary_.items()])))

    return (features, labels, tfidf)

##########   even the size of the two sets in articlesDF   ###########
# # randomly select articles so each has category has equal counts

import pandas as pd
from collections import Counter

def stratifiedSample(articlesDF, grouping='category'):  
    c = dict(Counter(articlesDF['category']))
    minc = min([(x) for (i,x) in c.items()])
    categoryWeights = [(i, minc / x) for (i,x) in c.items()]

    articlesSelectDF = pd.concat([articlesDF[articlesDF['category'] == 0].sample(minc),
                                  articlesDF[articlesDF['category'] == 1].sample(minc)])
    return (articlesSelectDF, categoryWeights)

from sklearn.feature_selection import chi2
import numpy as np
import pandas as pd

def getKeyWords(features, labels, category_id, tfidf, N, ngram):
    features_chi2 = chi2(features, labels == category_id)
    indices = np.argsort(features_chi2[category_id])         # need to index by category_id
    feature_names = np.array(tfidf.get_feature_names())[indices]
    return [v for v in feature_names if len(v.split(' ')) == ngram][-N:]

def correlatedGrams(df, tfidf, features, labels, N):
    category_id_df = df[['category', 'category_id']].drop_duplicates().sort_values('category_id')
    category_to_id = dict(category_id_df.values)
        
    unigrams = [getKeyWords(features, labels, category_id, tfidf, N, 1) for _, category_id in sorted(category_to_id.items())]
    bigrams = [getKeyWords(features, labels, category_id, tfidf, N, 2) for _, category_id in sorted(category_to_id.items())]
    return [unigrams, bigrams]

#df, categoryWeights = stratifiedSample(articlesSelectDF)
#df['category_id'] = df['category'].factorize()[0]   # add classification variable
#correlatedGrams = correlatedGrams(df, tfidf, features, labels, 3)

# use k-fold cross-validation on several models

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import MultinomialNB

import matplotlib.pyplot as plt
import seaborn as sns

def validateModels(features, labels, models, model_names=None, CV=5):
    cv_df = pd.DataFrame(index=range(CV * len(models)))
    entries = []
    for i,model in enumerate(models):
        if model_names == None:
            model_name = model.__class__.__name__
        else:
            model_name = model_names[i]
        accuracies = cross_val_score(model, features, labels, scoring='accuracy', cv=CV)
        for fold_idx, accuracy in enumerate(accuracies):
            entries.append((model_name, fold_idx, accuracy))

    cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])

    sns.boxplot(x='model_name', y='accuracy', data=cv_df)
    sns.stripplot(x='model_name', y='accuracy', data=cv_df, size=8, jitter=True, edgecolor="gray", linewidth=2)
    plt.show()
    
    return cv_df

# https://tatwan.github.io/How-To-Plot-A-Confusion-Matrix-In-Python/

from sklearn.metrics import f1_score
from sklearn.metrics import precision_score, recall_score

def plotConfusionMatrixWithStats(y_observed, y_predict, title):
    import matplotlib.pyplot as plt
	
    cm = metrics.confusion_matrix(y_observed,y_predict)
    pctCorrect = [[i,cm[i][i] / sum(cm[i])] for i in range(2)]
    plt.clf()

    plt.figure(figsize=(10,5))
    plt.subplot(121)
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Wistia)
    classNames = ['Negative','Positive']
    rowClassNames = [classNames[0] + '\n' + str(round(pctCorrect[0][1],3)) + ' %',classNames[1] + '\n' + str(round(pctCorrect[1][1],3))+ ' %']
    plt.title(title)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    tick_marks = np.arange(len(classNames))
    plt.xticks(tick_marks, classNames, rotation=45)
    plt.yticks(tick_marks, rowClassNames)
    s = [['TN','FP'], ['FN', 'TP']]
    for i in range(2):
        for j in range(2):
            plt.text(j,i, str(s[i][j])+" = "+str(cm[i][j]))
    
    plt.subplot(122)
    recall = recall_score(y_observed, y_predict)
    precision = precision_score(y_observed, y_predict)
    f1 = f1_score(y_observed, y_predict)
    cmt = cm.transpose()
    msg1 = '{} observations:\n{} negative + {} positive'.format(sum(sum(cm)),sum(cmt)[0],sum(cmt)[1])
    msg2 = 'predictions:\n{} negative + {} positive'.format(sum(cm)[0],sum(cm)[1])
    msg3 = 'recall = {}\nprecision = {}\nf1 = {}'.format(round(recall,3), round(precision,3), round(f1,3))
    plt.plot([[0]])
    plt.text(0,0, '{}\n\n{}\n\n{}'.format(msg1,msg2,msg3), ha='center', va='center')
    tick_marks = np.arange(0)
    plt.title('statistics')
    plt.xticks(tick_marks, '')
    plt.yticks(tick_marks, '')
 
    plt.show()

def plotConfusionMatrix(cm, title):
    import matplotlib.pyplot as plt
    
    pctCorrect = [[i,cm[i][i] / sum(cm[i])] for i in range(2)]
    plt.clf()
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Wistia)
    classNames = ['Negative','Positive']
    rowClassNames = [classNames[0] + '\n' + str(round(pctCorrect[0][1],3)) + ' %',classNames[1] + '\n' + str(round(pctCorrect[1][1],3))+ ' %']
    plt.title(title)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    tick_marks = np.arange(len(classNames))
    plt.xticks(tick_marks, classNames, rotation=45)
    plt.yticks(tick_marks, rowClassNames)
    s = [['TN','FP'], ['FN', 'TP']]
    for i in range(2):
        for j in range(2):
            plt.text(j,i, str(s[i][j])+" = "+str(cm[i][j]))
    plt.show()

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics

class TextToTfidfMatrix:
    
    from sklearn.feature_extraction.text import CountVectorizer
    from sklearn.feature_extraction.text import TfidfTransformer
    
    def __init__(self, features_train):
        self.count_vect = CountVectorizer(min_df=5 , max_df=.95, stop_words='english')
        self.tfidfTransform = TfidfTransformer()
        X_train_counts = self.count_vect.fit_transform(features_train)
        self.X_train = self.tfidfTransform.fit_transform(X_train_counts)
    
    def trainingTextMatrix(self):
        return self.X_train
    
    def textToTfidfMatrix(self, features_test):
        X_test_counts = self.count_vect.transform(features_test)
        X_test = self.tfidfTransform.transform(X_test_counts)
        return X_test
    
    def trainAndTestMatrices(self, features_test):
        return self.X_train, self.textToTfidfMatrix(features_test) 
    
    def tfidf(self):
        return self.tfidfTransform
    
    def vocabulary(self):
        return self.count_vect.vocabulary_

######################################################################################################
# list coefficients for logistic regression and plot histogram of values

from sklearn import preprocessing
from sklearn.svm import SVC
import numpy as np
import math

#from classificationFunctions import *   # correlatedGrams() fails unless I reload function library

def coefficientHistogram(clf):
    import matplotlib.pyplot as plt
    
    a = np.array(clf.coef_)[0]  # coefficients
    
    print(str(sum([1 for x in a if x != 0])) + ' non-zero coefficents')
    print('coefficients ranges from ' + str(min(a)) + ' to ' + str(max(a)))
    plt.hist(a, bins=15)  # arguments are passed to np.histogram
    plt.title("Coefficients for logistic regression")
    plt.show()

def getWordWeights(clf, select):
    vocab = dict([(c,x) for (x,c) in select.count_vect.vocabulary_.items()])
    nonZeroWords = [(c,x, vocab[x]) for (x,c) in enumerate(list(clf.coef_[0])) if c != 0]
    return sorted(nonZeroWords, key=lambda x: x[0])

def showMostDiagnosticWords(select, clf):
    wordWeights = getWordWeights(clf, select)
    listLength = math.floor(max(1,min(10,1+len(wordWeights)/2)))
    
    print(pd.concat([pd.DataFrame(wordWeights[:listLength], index=range(listLength), columns=['bottom coef','id','word']),
               pd.DataFrame(wordWeights[-listLength:], index=range(listLength), columns=['top coef','id','word'])], axis=1))

def plotProbErrorDist(clf, X_test, y_test):
    import matplotlib.pyplot as plt
    
    predictProb = clf.predict_proba(X_test)
    predictLogProb = clf.predict_log_proba(X_test) 
    
    import matplotlib.pyplot as plt
    xval = [list(y_test)[i] for i in range(len(y_test)) if list(y_test)[i] != predicted[i]]
    yval = [predictProb[i][1] for i in range(len(y_test)) if list(y_test)[i] != predicted[i]]
    
    plt.scatter(xval,yval)
    plt.title('errors cases only')
    plt.xlabel('observed')
    plt.ylabel('model probability')
    plt.show()

#   roc curve
	
from sklearn.metrics import roc_curve, auc

def plotROC(X_test, y_test, clf, title, decision_function_values):
    import matplotlib.pyplot as plt
    roc = metrics.roc_curve(list(y_test), decision_function_values) #clf.decision_function(decision_function_test))
    auc = metrics.auc(roc[0], roc[1])
    plt.title('{}: auc={}'.format(title, round(auc,4)))
    plt.plot(roc[0], roc[1])
    plt.xlabel('false alarms')
    plt.ylabel('hits')
    plt.show()
    
    return roc

def getDecision_function_values(clf, X):
    if clf.__class__.__name__ == 'LinearSVC':
        return clf.decision_function(X)
    else:
        return [x[1] for x in clf.predict_proba(X)]   # MultinomialNB

def bestDecisionPoint(roc, w0=1, w1=1):  # find the best point on the ROC curve based on weighted sum of corrects
    maxsum = (1 - roc[0][0])*w0 + roc[1][0]*w1
    for i,x in enumerate((1 - roc[0])*w0 + roc[1]*w1):
        if x > maxsum:
            maxsum = x
            ibest = i
    return ibest
				
def displayModelStats(clf, X_train, y_train, X_test, y_test, title, title2=' on test data'):
    plotConfusionMatrix(metrics.confusion_matrix(y_train, clf.predict(X_train)), title +' on training data')
    plotConfusionMatrix(metrics.confusion_matrix(y_test, clf.predict(X_test)), title + ' on ' + title2)
    return plotROC(X_test, y_test, clf, title + ' ' + title2, getDecision_function_values(clf, X_test))

def getPrediction(clf, select, text):
    X_test = select.textToTfidfMatrix(text)
    y_predict = clf.predict(X_test)
    return (y_predict)
