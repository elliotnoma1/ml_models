from classificationFunctions import *
import pickle, pandas

if __name__ == '__main__':
    clf, select = pickle.load(open('NBmodelFrom6kArticles.pickle', 'rb'))
    text_articles = pandas.read_csv('url_abstracts_final.csv')
    y_predict = getPrediction(clf, select, text_articles['Contents'])
    
    print('{} signals out of the set of {} articles'.format(sum(y_predict),len(y_predict)))
    plotConfusionMatrix(metrics.confusion_matrix(text_articles['Relevant/Non-Relevant'],y_predict), 'confusion matrix')
    showMostDiagnosticWords(select, clf)  
